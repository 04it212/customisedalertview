//
//  CustomAlertViewController.m
//  CustomisedAlertView
//
//  Created by snm on 7/2/12.
//  Copyright (c) 2012 snm. All rights reserved.
//
//customisedalertviewcontroller
#import "CustomAlertViewController.h"

@interface CustomAlertViewController ()
@end

@implementation CustomAlertViewController

@synthesize alertDelegate;
@synthesize window;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtons:(NSMutableArray *)otherButtonsArray
{
    self.view.frame = CGRectMake(100, 200, 500 ,600);
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(self.view.frame.size.width-100, self.view.frame.size.height-100, 100, 50);
    [cancelButton setTitle:cancelButtonTitle forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor blueColor];
    cancelButton.tag = CANCEL_BUTTON_INDEX;
    [cancelButton addTarget:self action:@selector(cancelButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:cancelButton];
    
    int x = cancelButton.frame.origin.x;
    int y = cancelButton.frame.origin.y;
    
    
    NSInteger index = 0;
    for(NSString *buttonTitle in otherButtonsArray)
    {
        UIButton *otherButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        otherButton.backgroundColor = [UIColor blackColor];
        [otherButton setTitle:buttonTitle forState:UIControlStateNormal];
        otherButton.titleLabel.textColor = [UIColor whiteColor];
        otherButton.tag = index++;
        
        otherButton.frame = CGRectMake(x-120, y, 100, 50);
        x = otherButton.frame.origin.x;
        y = otherButton.frame.origin.y;
        
        [otherButton addTarget:self action:@selector(otherButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:otherButton];
    }
    
    self.view.backgroundColor = [UIColor grayColor];
    
    return self;
}
-(void)otherButtonTapped:(id)sender
{
    
    UIButton *tappedButton = (UIButton *)sender;
    [alertDelegate buttonClickedAtIndex:tappedButton.tag];
}
-(void)cancelButtonTapped
{
    [self.window resignKeyWindow];
    self.window.hidden = YES;
    [alertDelegate cancelButtonClicked];
}

-(void)show
{
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.window.windowLevel = UIWindowLevelAlert;
    self.window.backgroundColor = [UIColor clearColor];
	
	//self.view.center = CGPointMake(CGRectGetMidX(self.window.bounds), CGRectGetMidY(self.window.bounds));

    //[self.window addSubview:self.view];
	[self.window setRootViewController:self];
	[self.window makeKeyAndVisible];
}


@end
