//
//  CustomAlertViewController.h
//  CustomisedAlertView
//
//  Created by snm on 7/2/12.
//  Copyright (c) 2012 snm. All rights reserved.
//

#import <UIKit/UIKit.h>

#define CANCEL_BUTTON_INDEX 0

@protocol AlertDelegate <NSObject>

@optional
-(void)cancelButtonClicked;
-(void)buttonClickedAtIndex:(NSInteger)index;

@required


@end
@interface CustomAlertViewController : UIViewController

@property (nonatomic,weak) id<AlertDelegate> alertDelegate;
@property (nonatomic,strong) UIWindow *window;
-(id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtons:(NSMutableArray *)otherButtonsArray;
-(void)show;
@end
