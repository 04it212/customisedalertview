//
//  ViewController.m
//  CustomisedAlertView
//
//  Created by snm on 7/2/12.
//  Copyright (c) 2012 snm. All rights reserved.
//

#import "ViewController.h"
#import "CustomAlertViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSLog(@"log before %i ",[[[UIApplication sharedApplication] windows] count]);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)callAlert:(id)sender
{
    NSMutableArray *otherButtonsArray = [NSMutableArray array];
    [otherButtonsArray addObject:@"Continue"];
    [otherButtonsArray addObject:@"Don't remind again"];
    
    
    CustomAlertViewController *customAlertView = [[CustomAlertViewController alloc] initWithTitle:@"Data Usage Alert" message:@"Please be careful about data usage" delegate:self cancelButtonTitle:@"Cancel" otherButtons:otherButtonsArray];
    customAlertView.alertDelegate = self;
   [customAlertView show];
    
    //[self.view addSubview:customAlertView.view];

    
    
}

-(void)buttonClickedAtIndex:(NSInteger)index
{
    NSLog(@"button tapped %i ",index);
}

-(void)cancelButtonClicked
{
    NSLog(@"cancel button clicked");
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

@end
