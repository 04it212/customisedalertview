//
//  ViewController.h
//  CustomisedAlertView
//
//  Created by snm on 7/2/12.
//  Copyright (c) 2012 snm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertViewController.h"
@interface ViewController : UIViewController <AlertDelegate>
{
}

-(IBAction)callAlert:(id)sender;
@end
